# Juego STOP!

Estimados tripulantes, vamos a poner a prueba un repositorio de Gitlab, en este caso por medio del recordado juego STOP!.

## Temas a desarrollar

1. Desarrollo de trabajo colaborativo usando `git pull` y `git push`
2. Descargas de proyectos por medio de `git clone`
3. Significado y uso de sync de Visual
4. Auto-merge en git
5. Local vs Remoto

## Como empezar

1. Cada tripulante clonará el proyecto STOP!
2. Cada tripulante creará su propia hoja en *markdown* en la carpeta del equipo que le corresponda
3. Los tripulantes recibiran instrucciones sobre como desarrollar tablas en markdown y la dinámica del juego.
4. El formador moderará el juego, cuando empiece el juego refrescará seguidamente gitlab para saber quien realizó el primer commit.
5. Apenas se diga STOP, todos deberan sincronizar el repositorio así no haya terminado de llenar la correspondiente fila. sin trampas!
6. Al terminar una letra y el primer STOP!, despues de que todos sincronicen el repositorio, cada quien saca su puntaje. Usar el microfono o el chat para saber si se repitió una palabra o no.
7. El puntaje se coloca al final de la tabla y luego se sincroniza el repositorio.
8. Se repite otra vez el juego para otra letra.


## Ejemplo de tabla

Nombre | Apellido | Ciudad | Fruta | Color | Animal | Cosa | Total
------ | -------- | ------ |------ | ----- | ------ | ---- | -----
Carlos | Castro | Cucuta | Coco |  | Cocodrilo | Carro | 600


## Como debe ser el mensaje del commit

Sabiendo que el commit guarda fecha, hora y autor, es importante tener bien configurado el nombre. El mensaje del commit debe terner la letra que uso para el juego. Ejemplo: "a"
